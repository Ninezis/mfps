// Fill out your copyright notice in the Description page of Project Settings.


#include "MFPS_Character.h"

// Sets default values
AMFPS_Character::AMFPS_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMFPS_Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMFPS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMFPS_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

